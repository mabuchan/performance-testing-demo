# This file should contain all the record creation needed to seed the database with its default values.
# The data can then be loaded with the rake db:seed (or created alongside the db with db:setup).
#
# Examples:
#
#   cities = City.create([{ name: 'Chicago' }, { name: 'Copenhagen' }])
#   Mayor.create(name: 'Emanuel', city: cities.first)

(1..1000).each do |n|
  price = rand * 1000
  quantity = (rand * 1000).to_i
  name = "Item #{n}"
  InventoryItem.create(name: name, quantity: quantity, price: price)
end
